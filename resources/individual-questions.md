---
title:  "Individual Reflection Questions"
type:   "Writing"
layout: "default"
---

## Personal Reflections

The theme of personal reflections are they are about specific people's practice. The particular questions may not apply to *you*, but perhaps there is some unasked question that *does* apply to you.

* How could I have influenced the piece with more complex lighting design?
* How could I have further expanded upon my code to make the lights respond to pitch and roll?

## Contributions

* What contributions do I feel I brought to the group?
* Am I invested enough into this project? 
* How should I assess my contribution to the team, what can I do better?
* How could I have felt like I had a greater impact on the project?
* How could I have been more beneficial to this project? 
* Did I contribute all that I could have as an individual to the project/ what could I have done better?

## Interdisciplinarity
* Should I be more focused on the process of discovering our theme, or it's better for me to focus on programming? Is it better for me to do both and not be so good at either, or to focus on one and do a better job?
* Do you see the dancer as a piece of the technology, or as different beings?


## Coordination
* How could individuals work separately on this project to best ensure the efficiency of the actual group work? Google Docs with assignments dividing work or is all work better done in a group?
* Did I do enough to listen to everyone's ideas?
* Is it possible to satisfy everyone? 
* How could I have better assessed my groups work before the performance?
* What should I keep in mind when designing code with a group versus when it's just me? (What are the different outcomes?) 

## Growth / Learning
* How could the CPx lights have been further developed knowing what I know now?
* What is my role as the programer in a collaborative dance project?
* What could I have added?
* How could I have made the dance part and the DCS part integrate smoother?
* What did this project do for you?
* What role did you individually play in adding value to your group?
* How could I have pushed myself more computationally to make this presentation better for the audience?

## Answers To Life's Persistent Questions

I've answered a few for you.

* If a spectator picks up on a meaning previously not intended by the creator, is this considered luck or brilliance? 

**Yes.**

* Could my performance incorporated another entirely new piece of technology?

**Also Yes.**

* Did the introduction of us lip-synching have meaning to the performance?

**Probably.**

* Did it make sense to have our entire group come on the stage at the end of the scene?

**No. But yes.**

* Did my program do what it was intended to do during the performance?

**Possibly. Possibly not.**

* What could I do better to push certain group members to participate? 

Humor aside, this is really hard. When someone is unwilling or unable to contribute, it's difficult to get them to engage. Consider: many students need more support in their learning, but they won't reach out to the professor. Why? It goes against their best interests... but, they would sooner struggle alone than ask for help. 

As a faculty member, there is only so much I can do to "push" students to (say) engage with the reading and assignments in a meaningful way (as opposed ot "just good enough"), to ask questions and engage actively in discussions in class, to set up and/or come in for office hours when help is needed, etc. It's a group process, and I have an explicit lead role... but, something in the system is broken, because the only tool I have is "assessment" at the end. A grade. It's a horrible motivator.

There is literature on group process and how to lead groups. It is not something you can learn quickly. And, until you've worked on a substantial group process, much of the literature *makes no sense*. It is not until you've had a chance to engage in something large and complex that you begin to see the difficulty of 1) motivating people, 2) keeping them coordinated, and 3) moving towards your goal (or goals) in a 4) timely manner. It's *really hard*. 

I recommend Bacon's [The Art of Community](http://www.artofcommunityonline.org/) as a starting point. I will consider integrating it into future projects of this sort, but at the end of the day... if I assign it, what percentage of you will read it?

Hm.

