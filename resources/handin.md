---
title: Using Handin
layout: default
---

Handin was written by Prof. Jadud. When something doesn't work, write him. He's the one who has to fix it.

Probably quickly, because there's almost certainly *other* people who want it fixed besides you.

<div class="text-center" style="margin-bottom: 20px;">
  <a href="http://handin.lynxruf.us/login">http://handin.lynxruf.us/login</a>
</div>

## Getting your "Password App"

First, you need to download a "two-factor" app for your phone, or install one in your browser.

* Authy has been tested, and is known to work ([Android](https://play.google.com/store/apps/details?id=com.authy.authy), [iPhone](https://itunes.apple.com/us/app/authy/id494168017), [Chrome](https://chrome.google.com/webstore/detail/authy/gaedmjdfmmahhbjefcbgaolhhanlaolb?hl=en)). You can find it in the phone app stores, and it comes in a version that works in your web browser.

* Google Authenticator can be used, but it gets low reviews on the iPhone. Untested.

* FreeOTP has been tested, and I swear, it gives me different numbers than Authy. I have not figured this out yet. If you use it, and it works, great.

If you want to learn more about 2FA, you can [watch videos like this one](https://youtu.be/0mvCeNsTa1g), or ask Prof. Jadud. 

Once you have this app installed, proceed.

## Getting your "Password"

We are actually not using 2FA correctly, but it solves a problem I have: how can I let you submit files securely without using your Bates password? Nifty trick, that.

First, visit this URL:

http://handin.lynxruf.us/secret/dvader@bates.edu

Except... you need to change Darth Vader's email address to *your* address. For example, I would copy-paste that URL into my Browser's URL bar, and then change it to:

http://handin.lynxruf.us/secret/mjadud@bates.edu

**This will only work once**.

You will be presented with a QR Code, or "2D barcode". 

1. Open Authy, click on the "..." at the top, and select "Add Account." 
1. Then, click **Scan QR Code**.
1. Scan the QR code.

Done! Close that tab.

## Submitting Work

At the bottom of assignment pages are big submission boxes. Drag your files into the box, and click upload.

You'll be asked for your email and your 2FA token. Open up Authy, select the handin.lynxruf.us account, and type in the number that is on the screen.

If all goes well, you'll see a checkmark on your files. If you see anything else, it means my code failed you, I failed you, and all things bad are happening in the universe. (If you don't see checkmarks on your files, then you should definitely send me an email.)

## Verifying Submission

You should be automatically redirected to the Handin site. You *may* need to enter another 2FA token. If that is the case, do so.

You'll get a list of files you have submitted. You should be able to see your most recent submissions at the top. If you see your files, then everything is good.

## Checking Assessment

When I assess work, it will show up on the "Assessment" page. You can get to that wit the Assessment link on the left-hand side of the page.

