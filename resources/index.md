---
title: Resources
layout: default
---

## Get Support

* My [office hour booking calendar](https://calendar.google.com/calendar/selfsched?sstoken=UUdVUXdIeWJ2R2ROfGRlZmF1bHR8ZjBiOGYxNjJjYTNmYmMwMDA5MzJjM2RjZmY1YzkxNjY) (or, by appointment; just drop me a note)
* The [TA evening/support schedule](tas)
* The [Question Form](https://goo.gl/forms/1VlLDe4t7l9J2ETP2). This form sends email to the prof and TAs; it's a great way to send a homework-type question to everyone, so that you might get a quicker response.

## Handing Things In

* [Here's how to use Handin]({{site.baseurl}}/resources/handin/).

## Makecode Links
* [Makecode for the CPx](https://makecode.adafruit.com/)
* [Makecode Guide @ Adafruit](https://learn.adafruit.com/adafruit-circuit-playground-express/makecode)
* [From Makecode to Python]({{site.baseurl}}/cp/) <br>
  A resource I've developed to help you 1) learn Makecode and 2) use that knowledge to translate it into Python.

## CPx Links
* [CPx Product Page](https://www.adafruit.com/product/3333)

## Python Links
* [Adafruit Getting Started Guide](https://learn.adafruit.com/adafruit-circuit-playground-express)


## Cool Random Stuff
*This section may need to be broken out and curated better.*

* [Make your own fabric bend sensor](http://www.instructables.com/id/Fabric-bend-sensor/)
* [Imogen Heap: Coposition Gloves](https://youtu.be/ci-yB6EgVW4) <br>
  A description of the gloves that Imogen uses for composition
* [Imogen Heap: Performance](https://youtu.be/6btFObRRD9k)<br>
  The performance starts around 13m30s in. 


## Course Blogs
* [https://hzhomecourt.wordpress.com](https://hzhomecourt.wordpress.com)
* [https://gracehanningxu.wordpress.com](https://gracehanningxu.wordpress.com)
* [https://lucydoesdcs.wordpress.com](https://lucydoesdcs.wordpress.com)
* [https://nicklynch10.wordpress.com](https://nicklynch10.wordpress.com)
* [https://josiahkrul.wordpress.com](https://josiahkrul.wordpress.com)
* [https://flan342939882.wordpress.com](https://flan342939882.wordpress.com)
* [https://nelsongdcs.blogspot.com](https://nelsongdcs.blogspot.com)
* [https://ecdcs102.wordpress.com](https://ecdcs102.wordpress.com)
* [https://pnofficial.wordpress.com](https://pnofficial.wordpress.com)
* [https://callmemaymay.wordpress.com](https://callmemaymay.wordpress.com)
* [https://jakeshapirodcs102.wordpress.com](https://jakeshapirodcs102.wordpress.com)
* [https://carolinemacclancy.wordpress.com](https://carolinemacclancy.wordpress.com)
* [https://rowannyourboat.wordpress.com](https://rowannyourboat.wordpress.com)
* [https://bobcatslife.wordpress.com](https://bobcatslife.wordpress.com)
* [https://prestonsblog2021.wordpress.com](https://prestonsblog2021.wordpress.com)
* [https://msomkuti.wordpress.com](https://msomkuti.wordpress.com)
* [https://dcs102.wordpress.com](https://dcs102.wordpress.com)
* [https://olalipop.wordpress.com](https://olalipop.wordpress.com)
* [https://tuckerwrites.wordpress.com](https://tuckerwrites.wordpress.com)
* [https://grainsofsaltmh.wordpress.com](https://grainsofsaltmh.wordpress.com)
* [https://pillaroftime.wordpress.com](https://pillaroftime.wordpress.com)
* [https://www.fahimarts.wordpress.com](https://www.fahimarts.wordpress.com)
* [https://ktowle2dcs.wordpress.com](https://ktowle2dcs.wordpress.com)
* [https://nolanreflectionsdcos.wordpress.com](https://nolanreflectionsdcos.wordpress.com)
* [https://blogarteplitz.wordpress.com](https://blogarteplitz.wordpress.com)
* [https://102reasonstodcs.wordpress.com](https://102reasonstodcs.wordpress.com)
* [https://nickbeati5.wordpress.com](https://nickbeati5.wordpress.com)
* [https://college952477838.wordpress.com](https://college952477838.wordpress.com)
* [https://andrewsdcsblog.wordpress.com](https://andrewsdcsblog.wordpress.com)
* [https://kenzadcsblog.wordpress.com](https://kenzadcsblog.wordpress.com)
* [https://college952477838.wordpress.com](https://college952477838.wordpress.com)
* [https://colbysdcsblog.wordpress.com/](https://colbysdcsblog.wordpress.com/)

----

*These aren't done baking yet...*
## Software
* The DCS102 CPx Library 

## Videos
* Setting Up the CPx (Circuit Playground Express) for Python
* Writing your first Python program
