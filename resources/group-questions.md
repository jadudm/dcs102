---
title:  "Group Reflection Questions"
type:   "Writing"
layout: "default"
---

These are reflection questions that you have generated as a group. I have grouped them into rough categories. You may believe a question belongs in a different category, or cuts across multiple categories: *that's OK*. My categorization is merely a possible aid/guide to help you connect the questions in your own mind, but it is neither absolute, correct, nor sacrosanct.

They are numbered for ease of reference only.

## Interconnection 

1.  How could we have better worked as a group on the interdisciplinary level to more smoothly incorporate the tech, music, and dance theme?
1.  How could we further develop our theme?
1.  How could we have shown our ideas more clearly without explicitly saying what is going on?
1.  What could we do to increase the interdependence of the choreo/tech/music?
1.  How could our idea have been more functional when translated to dance?
1.  Could our group have better communicated the importance of dance-music relation? 
1.  Should every movement involve the sensors?
1.  Have we constructed the performance in such a way that it effectively conveys thematically what we generally want to the audience?
1.  Did our coding inhibit the choreographers from expanding their dance? 
1.  How much interaction between human and technology is needed to satisfy our minds into saying the piece is a cohesive unit with all parts?

## Process

1.  Should we develop the theme of the dance first and revolve music and digital component around it, or should they progress in parallel (which can cause many problems like we faced in terms of integration)?
1.  I feel like our performance is pieced together by many really cool but loosely connected fragments, how to integrate them more and does our sequence of discussion and work contributed to this fragmentation?
1.  What was the role of the programmers in the group vs. the musicians, and how could their jobs be better related to each other?
1.  How could we have made all the parts of our project gel together better?
1.  Did all of our participants contributing add to the performance?
1.  Could the groups separated and joined with one another to discuss joint ideas?
1.  How could the DCS members add more value to the dance and music aspect of the performance?
1.  Is it best to try and work together as one big group or in smaller groups with occasional check-ins?
1.  Did everyone in the group actively engage and participate when working together? 
1.  Would groups benefit from a designated 'group leader'?
1.  Many of the criticisms noted were things our group were aware of/trying to address. How could we have worked better to address these concerns more effectively?

## Learning

1.  How could we have further developed our projects, specifically with the coding knowledge that we have?
1.  What was the purpose of this assignment?
1.  What should we have learned?
1.  Is there an additional discipline or dimension that could have been added other than dance, music, and technology?
1.  How did you overcome challenges as a group? 

## Performance

1.  How do we make the aspects of the performance seem intentional to the audience?
1.  What could we have added to make it more complex?
1.  How could we have made it more than a theme?
1.  Should other groups (musicians, tech leads) have joined in on the actual performance instead of leaving it to the dancers only?
1.  Did the group's music and CPX placement mesh well with their movements?
1.  How can we create a better use of/ incorporate the physical space used?

1.  How could our group more accurately convey the theme of our performance?
1.  Would a pre-performance narrative of our chest lights/theme/dance/backdrop help? 
1.  How was the light tied into the performance? 
1.  Whereas we as a group found the relationship between sound and dance very obvious, how could we have made this more obvious to the audience members?
1.  What do we hope the audience gains from this performance? 
1.  What do we want to convey and what are the intentions when reaching the audience? How are we reaching the audience (what/who are we appealing to in terms of ethos, pathos etc.)?

