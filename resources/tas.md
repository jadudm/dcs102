---
title: Teaching Assistants
layout: default
---

<div class="row" style="background-color: #DDD; margin-top: 40px;">
  <div class="col-3">
  </div>
  <div class="col-3">
    Week of 1/15
  </div>
  <div class="col-3">
    Week of 1/22
  </div>
  <div class="col-3">
    Week of 1/29
  </div>
</div>

<div class="row">
  <div class="col-3">
    <b>Monday, 8PM - 9PM</b>
  </div>
  <div class="col-3">
    Jasmine/Sam
  </div>
  <div class="col-3">
    Rebecca/Finton 
  </div>
  <div class="col-3">
    Christopher/Tim
  </div>
</div>

<div class="row">
  <div class="col-3">
    <b>Wednesday, 8PM - 9PM</b>
  </div>
  <div class="col-3">
    Tim/Malik
  </div>
  <div class="col-3">
    Lily/Sam
  </div>
  <div class="col-3">
    Malik/Jasmine
  </div>
</div>


<div class="row" style="background-color: #DDD; margin-top: 40px;">
  <div class="col-3">
  </div>
  <div class="col-3">
    Week of 2/5
  </div>
  <div class="col-3">
    Week of 2/12
  </div>
  <div class="col-3">
    Week of 2/19
  </div>
</div>

<div class="row" >
  <div class="col-3">
    <b>Monday, 8PM - 9PM</b>
  </div>
  <div class="col-3">
    Sam/Rebecca
  </div>
  <div class="col-3">
    Christopher/Jasmine
  </div>
  <div class="col-3">
    BREAK
  </div>
</div>

<div class="row">
  <div class="col-3">
    <b>Wednesday, 8PM - 9PM</b>
  </div>
  <div class="col-3">
    Lily/Finton
  </div>
  <div style="background-color: yellow;" class="col-3">
    <small>2/14 <b>ERICA MOTT PERFORMANCE</b></small>
  </div>
  <div class="col-3">
    BREAK
  </div>
</div>

<hr>
<div class="row" style="background-color: #DDD; margin-top: 40px;">
  <div class="col-3">
  </div>
  <div class="col-3">
    Week of 2/26
  </div>
  <div class="col-3">
    Week of 3/12
  </div>
  <div class="col-3">
    Week of 3/19
  </div>
</div>

<div class="row">
  <div class="col-3">
    <b>Monday, 8PM - 9PM</b>
  </div>
  <div class="col-3">
    Lily/Rebecca
  </div>
  <div class="col-3">
    Tim/Malik
  </div>
  <div class="col-3">
    Rebecca/Finton
  </div>
</div>

<div class="row">
  <div class="col-3">
    <b>Wednesday, 8PM - 9PM</b>
  </div>
  <div class="col-3">
    Finton/Sam
  </div>
  <div class="col-3">
    Christopher/Jasmine
  </div>
  <div class="col-3">
    Lily/Sam
  </div>
</div>


<hr>
<div class="row" style="background-color: #DDD; margin-top: 40px;">
  <div class="col-3">
  </div>
  <div class="col-3">
    Week of 3/26
  </div>
  <div class="col-3">
    Week of 4/2
  </div>
  <div class="col-3">
    <b>FINALS WEEK</b>
  </div>
</div>

<div class="row">
  <div class="col-3">
    <b>Monday, 8PM - 9PM</b>
  </div>
  <div class="col-3">
    Finton/Lily
  </div>
  <div class="col-3">
    Jasmine/Christopher
  </div>
  <div class="col-3">
    
  </div>
</div>

<div class="row">
  <div class="col-3">
    <b>Wednesday, 8PM - 9PM</b>
  </div>
  <div class="col-3">
    Sam/Rebecca
  </div>
  <div class="col-3">
    Tim/Malik
  </div>
  <div class="col-3">
    
  </div>
</div>
