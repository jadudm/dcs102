---
title: "Setting up the CPx"
heading: "Setup"
chapter: 1
section: 1
---

To use your CPx with Python, you will need to take a few steps. You should, of course, feel no fear in asking questions about getting things set up. 

## First, Circuit Python

There is [a wealth of information on the Adafruit website](https://learn.adafruit.com/adafruit-circuit-playground-express?view=all), but we're going to simplify things a bit here. First, you are going to need to install Python on your CPx.

[Right-click and download Circuit Python for the CPx](https://github.com/adafruit/circuitpython/releases/download/2.2.0/adafruit-circuitpython-circuitplayground_express-2.2.0.uf2). This is version {{site.circuitpython.version}}, which is what we're using this term.

Then, plug in your CPx, hit the reset button (to put it into CPLAYBOOT mode), and drag the UF2 file onto the CPx. The red led should blink to indicate that the file is transferring.

## Next, the Library of Amazing Code

Code comes in *libraries*, just like books. A library of code is a collection of programs that you can make use of. Just like other people write books (that you can read and learn from), you can use a library of code to simplify your own programs, by using code others wrote.

[Right click and download the Adafruit CPx Libraries](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/download/20180119/adafruit-circuitpython-bundle-2.2.0-mpy-20180119.zip). Unzip that file, and drag-and-drop the *libs* folder onto the CPx.

Then, download the DCS library. Do this by going to the [release page](https://github.com/jadudm/circuitpythonexpress-dcs/releases) and downloading the most recent release (at the top of the page). It will be a zip file. Unzip the file, open the folder, and then drag the file **dcs.py** onto the CPx. You *only* need the **dcs.py** file. Note that I will update this file from time-to-time to meet your needs, and we may have to download it again.

If you want to use the microphone, you should drag the file *microphone.py* onto your CPX. However, it takes a lot of the memory of the CPX, so code that uses the microphone cannot *also* use the *dcs.py* library at the same time. Either more work needs to be done to reduce the memory requirements of the microphone library, or we need a CPX with more RAM. (That will happen, in time.)

## Finally, an Editor

The tool we use to edit code is called an *editor*. You use it to... edit code.

We'll make use of Mu. You should download the [Mac](https://github.com/adafruit/mu/releases/download/1.0.0b13/macosx_mu.zip) or [Windows](https://github.com/adafruit/mu/releases/download/1.0.0b13/mu.exe) version.

On the Mac, you *must* be running macOS 10.12 or higher; you will get an error on macOS 10.11 and further back. Let your Mac update.

Once you've unzipped things on the Mac, draw the application "mu" to your Applications folder. Then, right-click on it, and say "Open." This will open Mu for the first time. (If your CPx is not plugged in, Mu will complain.) You can also drag Mu into your Dock, because you'll be using it all the time.

I have no idea what you should do on Windows, and I would welcome a student writing some text for me to paste in here.

## Ready, Set...

You should be good to go. To test your setup, you should be able to copy-paste this program into Mu, save it as "main.py" on your CPx, and watch it animate the pixels.

```python
from dcs import *
from random import randint

setup_pixels(brightness=1)

ndx = 0

while True:
    set_pixel(ndx % 10, randint(0,255), randint(0,255), randint(0,255))
    ndx = ndx + 1
```