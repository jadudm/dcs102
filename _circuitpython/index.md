---
title: "From Makecode to Circuit Python"
---

## From Makecode to Circuit Python
### *Or*, The Busy Student's Introduction to Circuit Python

These resources introduce Makecode on the Adafruit Circuit Python Express (CPx), and guide you through to translating that Makecode into Python (or, more specifically, Circuit Python, the flavor of Python that runs on the CPx.)

All materials on this site are [licensed CC-BY and available in Bitbucket](https://bitbucket.org/jadudm/dcs102). If there is value in formatting them differently for accessibility, or for inclusion in other repositories, please contact <a href="mjadud@bates.edu">Matt</a> directly.

  <ul>
{% for chapter in (1..10) %}
  {% for section in (1..10) %}
    {% for cp in site.circuitpython %}
      {% if cp.chapter == chapter and cp.section == section %}
        {% if page.title == cp.title %}
          <li> <b>{{cp.title}}</b> </li>
        {% else %}
          <li> <a href ="{{site.baseurl}}{{cp.url}}.html">{{cp.title}}</a> </li>
        {% endif %}
      {% endif %}
    {% endfor %}
  {% endfor %}
{% endfor %}
</ul>
