---
title: "Programming Concepts in Makecode"
heading: "Setup"
chapter: 1
section: 2
---

You might be new to Makecode, and programming in general. These videos introduce fundamental structures and concepts in programming using Makecode.

## Variables

A variable is a container for a value. It is a way for us, as programmers, to give a name to a value so we can refer to it over and over. For example, we might take a reading from a sensor, store it in a variable, and refer back to that reading multiple times. If we were to read from the sensor multiple times, we might get different values; by storing it once, we get the same value every time we reference the variable.

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/Pgi8e3og-Cg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

## Conditionals

Conditionals let us make choices. "If" is a common conditional in every programming language you are likely to encounter. 

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/82iTOU9VsIw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

## Multi-Arm (or Multi-Branch) Conditionals

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/2RHpKxFuBCY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

## Loops and Indicies

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/EEal_1R0vjo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
