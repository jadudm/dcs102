---
heading: "Translating"
title: "The Rosetta Stone"
chapter: 3
section: 1
---

For this code to run on the CPx, you will need the <a href="{{site.dcslib}}">DCS Python Library</a>

## The Minimum

<div class="row">
  <div class="col-6">
    <img src="makecode/circuitplayground-empty.png" width="60%" class="img-fluid mx-auto d-block" />
  </div>
  <div class="col-6">
    
<pre><code class="language-python">from dcs import *

# On Start

# Forever
while True:
  pass
</code></pre>
    </div>
</div>

In Python, the '#' is a comment. That means the CPx will ignore anything you put after a '#'. So, really, the "start" block of your Python code is *anything* that comes before the "forever loop."

"while True" is a loop that never ends. It will run everything that you indent underneath it over-and-over-and-over-and-over. In this case, I put a "pass" in the loop, which means "do nothing."

<div style="margin-top: 20px;"> &nbsp; </div>

## Blinking the LED

If we want to turn on a pixel, read the value of the light sensor, or the accelerometer, we do that by invoking a *function*. Think of a function as one or more instructions that we've given a name. For example, if I told you to make me a sandwich, we might say that I was using the <code>pleaseGoMakeMeASandwich()</code> function. As a single command, it represents a whole bunch of actions: getting up, going to the fridge, getting out sandwich makings, etc. The name isn't particularly critical; perhaps we'd call it <code>please_go_make_me_a_sandwich()</code> instead of <code>pleaseGoMakeMeASandwich()</code>... both would still ammount to you (kindly) going to make me a tasty sandwich. The important thing here is that the *name* of the function doesn't change *what it does*. However, we do like it when a function's name is descriptive. 

We have a number of functions we can use in Circuit Python. Many, in fact. They are what let us do amazingly cool things. The code below uses two functions: one to turn all the pixels on or off, and one to pause for some number of milliseconds.


<div class="row">
  <div class="col-6">
    <img src="makecode/circuitplayground-pixels-on-off.png" width="70%" class="img-fluid mx-auto d-block" />
  </div>
  <div class="col-6">
    
<pre><code class="language-python">from dcs import *

# On Start
setupPixels()

# Forever
while True:
  setAllPixelsTo(RED)
  pause(500)
  setAllPixelsTo(BLUE)
  pause(500)
</code></pre>
    </div>
</div>

Currently, there is only RED, GREEN, and BLUE.

<div style="margin-top: 20px;"> &nbsp; </div>

## Initializing a Variable

A variable is a container that holds a value. I've sometimes said it is like a cardboard box with stuff in it; the *value* of the variable is the stuff in the box. The *name* of the variable is a word you scribble on the outside of the box with a marker.

We will be making the habit of initializing (meaning *setting to an initial value*) any variables we are going to use in our programs.

<div class="row">
  <div class="col-6">
    <img src="makecode/circuitplayground-variabletozero.png" width="90%" class="img-fluid mx-auto d-block" />
  </div>
  <div class="col-6">
    
<pre><code class="language-python">from dcs import *

# On Start
sensor_reading = 0

# Forever
while True:
  pass
</code></pre>
    </div>
</div>

<div style="margin-top: 20px;"> &nbsp; </div>

## Reading the Accelerometer



<div class="row">
  <div class="col-5">
    <img src="makecode/circuitplayground-accelX.png" width="90%" class="img-fluid mx-auto d-block" />
  </div>
  <div class="col-7">
    
<pre><code class="language-python">from dcs import *

# On Start
accel_reading = 0

# Forever
while True:
  # Get the X-axis accelerometer reading, and 
  # store it in the variable "accel_reading"
  accel_reading = getAccel(XAXIS)
</code></pre>
    </div>
</div>

The name of the variable does not change what is stored in it. In the example below, I have kept the variable name the same, and have grabbed the Y-axis reading from the acceleromter, instead of the X-axis reading.

<div class="row">
  <div class="col-5">
    <img src="makecode/circuitplayground-accelY.png" width="90%" class="img-fluid mx-auto d-block" />
  </div>
  <div class="col-7">
    
<pre><code class="language-python">from dcs import *

# On Start
accel_reading = 0

# Forever
while True:
  # Get the Y-axis accelerometer reading, and 
  # store it in the variable "accel_reading"
  accel_reading = getAccel(YAXIS)
</code></pre>
    </div>
</div>

<div style="margin-top: 20px;"> &nbsp; </div>

## Storing Two Sensor Readings

In the previous example, you saw how you could read, and store, a sensor reading. Then, I changed the sensor I was storing. Here is how I would read, and store, two sensor readings in two different variables.


<div class="row">
  <div class="col-5">
    <img src="makecode/circuitplayground-two-variables.png" width="90%" class="img-fluid mx-auto d-block" />
  </div>
  <div class="col-7">
    
<pre><code class="language-python">from dcs import *

# On Start
accel_x_reading = 0
accel_y_reading = 0

# Forever
while True:
  accel_x_reading = getAccel(XAXIS)
  accel_y_reading = getAccel(YAXIS)
</code></pre>
    </div>
</div>

## Asking Questions

The **if** block in Makecode lets you ask questions. In Python, we also ask questions using an if. The pattern is that you write "if", ask your question, finish the question with a colon, and then on the next line you indent four spaces. Everything that is indented under the **if** will happen *only* if the question you ask is true.

The accelerometer on the CPx behaves differently under Python than it does under Makecode. Under Python, it gives you actual readings in gravities. That means that the Z axis should, when sitting on a table, give you a reading of -9.8 gravities. 

The "print" function will let you see values on the REPL in Mu. Very handy for exploring and experimenting. There is no equivalent to "print" in Makecode.

<div class="row">
  <div class="col-5">
    <img src="makecode/circuitplayground-if0.png" width="90%" class="img-fluid mx-auto d-block" />
  </div>
  <div class="col-7">
    
<pre><code class="language-python">from dcs import *

# On Start
accel_x_reading = 0

# Forever
while True:
	# First thing in our loop: get a
	# reading from the accelerometer.
	accel_x_reading = getAccel(XAXIS)
	
	# Print and look at the reading
	print(accel_x_reading)
	
	# Then, if it is greater than 5, 
	# turn the pixels red. Otherwise,
	# turn them blue.
	
	if accel_x_reading > 5:
	    setAllPixelsTo(RED)
	else:
	    setAllPixelsTo(BLUE)
</code></pre>
    </div>
</div>

{% comment %}
## Title

Blurb


<div class="row">
  <div class="col-5">
    <img src="makecode/circuitplayground-two-variables.png" width="90%" class="img-fluid mx-auto d-block" />
  </div>
  <div class="col-7">
    
<pre><code class="language-python">from dcs import *

# On Start

# Forever
while True:
</code></pre>
    </div>
</div>
{% endcomment %}

