folders = [ "fun", "in-class", "plusone", "reading", "writing", "code" ]
typeof =  [ "Fun", "In Class", "Plus One", "Reading", "Writing", "Code" ]
shortcuts = ["f", "ic", "po", "r", "w", "c"]

import os
def touch(path):
    with open(path, 'a'):
        os.utime(path, None)

def pad(n):
    if n < 10:
        return "0" + str(n)
    else:
        return str(n)


for f, sc, to in zip(folders, shortcuts, typeof):
    daynum = 4
    for w in [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]:
        for d in [1, 3, 4]:
            if f not in ["fun"]:
                daynum = daynum + 1
                inp  = open("_template.md")
                outp = open(f + "/" + "_" + sc + pad(w) + pad(d) + ".md", "w")
                for line in inp:
                    print (f + "/" + "_" + sc + pad(w) + pad(d) + ".md")
                    line = line.replace("DAYNUM", "Day " + str(daynum))
                    line = line.replace("DAY", str(d))
                    line = line.replace("WEEK", str(w))
                    line = line.replace("CATEGORY", to)
                    outp.write(line)
            else:
                inp  = open("_template.md")
                outp = open(f + "/" + "_" + sc + pad(w) + pad(d) + ".md", "w")
                for line in inp:
                    print (f + "/" + "_" + sc + pad(w) + pad(d) + ".md")
                    line = line.replace("DAYNUM", "End of Week " + str(w + 1))
                    line = line.replace("DAY", str(d))
                    line = line.replace("WEEK", str(w))
                    line = line.replace("CATEGORY", to)
                    outp.write(line)
