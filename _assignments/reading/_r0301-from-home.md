=======
title:  "Studying Programming 3, 5"
type:   "Reading"
slug:   "Making Mistakes and Writing Code"
rubrics: [reflection]
---

For Tuesday, we have two chapters of Studying Programming and a webpage.

* [Everyone Makes Mistakes](https://drive.google.com/open?id=0B4DhmK_4PXR8VW90V2Q4T0VzQ1k)

* [Writing your First Program](https://drive.google.com/open?id=0B4DhmK_4PXR8WW9XTHVFYXpkQU0)

These two chapters directly support our move into the learning of the Python programming language. There is no explicity reflection, but we will be leveraging the content of these chapters in our learning.

## A First Read: FMCtCP

I have begun writing a ["rosetta stone" that translates Makecode to Python]({{site.base}}/cp/rosetta). I'm calling it "From Makecode to Circuit Python," or perhaps it should be "A Busy Student's Guide to Circuit Python." The title, in truth, is not important.

After reading chapters 3 and 5, make a two lists:

1. Read through the pairs of programs on that page. Are there code examples that are particularly useful to you? If so, why? And, if there are examples that confused you, what were they, and why?

1. Read through the Python programs on that page. Given your readings of chapters 3 and 5, what kinds of coding mistakes look possible/likely? 

## Questions in Class

Come to class prepared to ask questions at the start of class that you have from the text. I will structure this as a short, small-group conversation around our lists, and then solicit questions from the whole group. 

>>>>>>> ecfe82b3cc9aebc56d594c19bb8d7d1094cc2a5f