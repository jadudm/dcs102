---
week:   "12"
day:    "1"
title:  "Debate Prep"
type:   "Reading"
slug:   "Because all that knowledge comes from somewhere..."
---

For Tuesday, you should be working with your group to prepare for the debate.

You have two possible debates you're developing material for. 

I would recommend the following:

1. Create a Google Drive folder.

2. Share it with everyone in the group.

4. Create a Doc called "Opening Statement - Psychographics"

6. Doc: "Research - Psychographics"

8. Doc: "Closing - Psychographics"

Now, as you do your work, the entire team will have access to everything.

You're going to want to decide when your group will complete things by. For example, I would do some research for Thursday, and I'll give you some time in class to touch base. You should then plan on having your statements done by Sunday, so that on Monday you can all look at each-other's work before Tuesday.

What you actually do is up to you and your group. This is all just me trying to be helpful, by describing the kind of structure *I* would want to see in place with *my* group.

