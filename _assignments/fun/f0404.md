---
week:   "4"
day:    "4"
title:  "Fun: Animusic"
type:   "Fun"
slug:   "And I thought the Virtual was cool..."
---

In 2004, the company Animusic released a 3D rendering demo, setting MIDI music to a rendered animation of an impossible instrument.

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/hyCIpKAIFyo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<div style="margin-top: 20px;"> &nbsp; </div>

In 2012, Intel *built* the damn thing.

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/JLdB0WEixjM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<div style="margin-top: 20px;"> &nbsp; </div>

Imagine building that thing with 250 CPxs, all talking to each-other, and getting the timing *exactly right*. That's kinda awesome.

Leading up to 2016, *Marble Machine* was built and composed by Martin Molin. He might be crazy. But, then again, crazy can be kinda awesome. Actually, I'm going to suggest *dedicated* instead of *crazy*. And, driven. And inspiring.

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/IvUU8joBb1Q" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<div style="margin-top: 20px;"> &nbsp; </div>

For the Marble Machine, imagine getting it all *exactly right*, but because you *built the machine exactly the right way*. No computers. That's awesome.

To finish, if you just think mechanical musical instruments are cool, Martin has recorded videos as he explores these wonders:

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/OsjG1aEdogw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<div style="margin-top: 20px;"> &nbsp; </div>

And, because who wouldn't want their music performed on a bell tower!

<div class="text-center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/6CQgzqYuo04" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>