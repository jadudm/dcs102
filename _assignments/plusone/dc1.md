---
week:   "11"
day:    "4"
hour:   "19"
minute: "30"
title:  "Spring Dance Concert - A"
type:   "Plus One"
slug:   "Our friends in dance!"
---

A final "+1" opportunity.

<div class = "text-center mx-auto d-block">
  <img src="{{site.baseurl}}/images/dance-concert.jpg">
</div>

Our friends in Dance have their final productions:

### Program A

* Friday, March 30, 7:30PM
* Sunday, April 1, 2PM

### Program B

* Saturday, March 31st, 5PM
* Monday, April 2nd, 7:30PM

Support your colleagues in dance. Many (all?) of the choreographers you worked with this term have performances in this concert!

