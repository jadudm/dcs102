---
week:   "4"
day:    "1"
title:  "Translating Makecode to Python"
type:   "Code"
slug:   "A first exploration"
---


First, I want to share with you a strategy.

1. Today is Thursday. Take Friday off from DCS stuff, unless you want to think about dance collaboration stuff.

2. On Saturday, sit down in the morning (or afternoon, or evening) when you're fresh, and get out your CPx and Mu. [Get the .uf2 file]({{site.uf2}}) (that *is* CircuitPython), and drag it onto the CPLAYBOOT drive. 

1. Get the [most recent version of the dcs.py file](https://github.com/jadudm/circuitpythonexpress-dcs/releases), and put it on the CPx.

3. Open up Mu.

4. Copy paste the program below into Mu.

5. Save your program to the CPx, and call it **main.py**. Make sure you're saving it to the CPx, and not some other folder on your computer.

6. It should do stuff.

**Stop**.

That's Saturday. If you get stuck on Saturday, send an email to me. I'm going to come in Sunday, and am happy to help debug at this stage.

1. On Sunday, if you're debugged and moving, I want you to try and write some code for the first question below. It should be a lot like the Rosetta Code example. If you felt good about that, try a second. If you get stuck or confused, *write me an email and ask questions*. I'll see if any TAs are willing to do a Sunday evening session to help make sure you're making progress.

1. Try again Monday. If you're making progress, finish the other two questions. Otherwise, *go to the TA hours on Monday evening*. If you can't, and are stuck, you are probably a bit late for asking questions, but I'll check email in the evening to see if I can help. The reality is that you're pushing it a bit late.

Come to class on Tuesday having done your best to do the following four questions. Bring questions that you came up with along the way. For example:

1. If you have code that isn't working, copy-paste it into a Google Doc. Even if you get it working, you still may have questions. So, bring the code to class, and ask questions. 

1. You may have questions about why things are the way they are. Ask those.

1. Or, you might have some other question. Ask.

### The Code To Test on the Mu

I referenced code up above that you should use as your starting point for testing. Here it is.

<div class="row">
  <div class="col-9">
<pre><code class="language-python">from dcs import *

# On Start
setupPixels()

# Forever
while True:
  setAllPixelsTo(RED)
  pause(500)
  setAllPixelsTo(BLUE)
  pause(500)
</code></pre>
    </div>
</div>

Give that code a try, and see if you can get it working. That's your first step. If it was easy, you might keep going. If it tried your patience, perhaps you'll step away for a bit. Or, perhaps you're ready to [*try and take over the world*](https://www.youtube.com/watch?v=GBkT19uH2RQ). 

## Questions

You have a strategy up above. I recommend using it. And, below, are the four questions I'd like you to approach this weekend.

1. **Blink Three Colors**. <br>
  Modify the program above to blink a third color (with a pause). GREEN is the only other color defined.
  
1. **Blink More Colors**. <br>
    All [colors on the CPX are defined by R, G, and B values](https://www.rapidtables.com/web/color/RGB_Color.html). So, you can make up your own colors with the `setAllPixelsToRGB` function.
  
    `setAllPixelsToRGB(0, 0, 0)`
  
    which would be "off." If you want the color red, you'd do
  
    `setAllPixelsToRGB(255, 0, 0)`  
  
    and green would be
  
    `setAllPixelsToRGB(0, 255, 0)`
  
    and you can guess blue. Write a program that blinks more colors.
  
1. **Explore the Accelerometer**. <br>
  Look at the [Rosetta Stone]({{site.baseurl}}/cp/rosetta.html). Start with the program that reads an accelerometer value, and asks a question. (It's near the bottom.) Tweak the number in the **if** statement and see what happens as you change it from 0 to 18. Change, save, explore. Change, save, explore.
  
1. **Face Up Detector**. <br>
  Can you modify the previous program so that the CPx turns RED when it is face up, and BLUE when it is face down?
    
1. **CHALLENGE: Nightlight**. <br>
  Write a program that, as it gets darker, the CPx gets brigher. It might be this is one question too many for your first time out. That's OK. It involves using setAllPixelsToRGB, and as a hint, the grayscale from off-to-bright-white involves keeping the R, G, and B values the *same*. That is, (0, 0, 0) is off, and (255, 255, 255) is white. Also, you'll need to use **elif**, which we haven't seen, and it isn't in the Rosetta Stone yet. You can look at [this page](https://www.tutorialspoint.com/python/python_if_else.htm) to see if you can puzzle it out. If not, don't fear. I'm challenging you a bit here.
  
## Submission

When you get a program that works, paste it into a Google Doc. Write me a few notes about what was challenging and/or what went went well for you on that particular program. If it still doesn't work, say so, but you really should have been *asking questions* if you were stuck, so the TAs and I can help you. There is **no shame** in asking questions; it is a critical part of learning.

Save the Doc as a PDF, and upload the PDF to Lyceum.
