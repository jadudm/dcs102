---
week:   "3"
day:    "1"
title:  "Setting Up Python"
type:   "Code"
slug:   "Take the first step."
---

<div class="float-right">
  <img src = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Rosetta_Stone.JPG/256px-Rosetta_Stone.JPG">
</div>

For Tuesday, we're going to take the work we did last week, and do it again. However, we're going to do it in Pyhton. The nice thing is that this is, in part, an exercise in *translation*. You've already solved the problems in one language (Makecode). Now, we'll circle around and solve them again, but in a different language.

To do the translation, I've provided a bit of a [Rosetta Stone](https://en.wikipedia.org/wiki/Rosetta_Stone). The Rosetta Stone is an ancient language-translation dictionary that we only partially understand... but it has helped researchers immensely in the study of ancient languages. This page is the start of a "Rosetta Stone" for Makecode and Python on the Circuit Playground Express.

You can find the [Rosetta Stone]({{site.baseurl}}/cp/) resource in with all of the other [Circuit Python]({{site.baseurl}}/cp/) resources on this site.

...

And, something about reading Makecode (functions)

And, some reading. Perhaps Studying Programming, or perhaps some SP and the Google Memo.
