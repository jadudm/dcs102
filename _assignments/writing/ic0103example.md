---
week:   "1"
day:    "3"
title:  "Handin Example"
type:   "Writing"
slug:   "Demo of Handin"
upload: true
rubric: [culturalopenness, syntaxandmechanics]
---

*Here, I write a lot of stuff about what you should be doing. You skim it, despite it taking me hours to write it.*

