---
week:   "9"
day:    "1"
title:  "Three Programs, Three Paragraphs"
type:   "Code"
slug:   ""
---

## A Point of Comparison

First, I would like you to look at the first two chapters of a well-known introductory text for Python.

* [Chapter 1: Think Python 2e](http://greenteapress.com/thinkpython2/html/thinkpython2002.html)
* [Chapter 2: Think Python 2e](http://greenteapress.com/thinkpython2/html/thinkpython2003.html)

Read the chapter. Your goal is not to memorize the chapter. Read it once to get a sense for the content. Then, read it a second time, and read *critically*, analyzing the things that are said, the order they are in, and the depth to which each subject, term, or idea is explored.

It may be that you've had enough programming experience that these things are starting to make sense to you. It may be that they're on the *edge* of making sense. It may be that this moves too fast, and you're saying "no, actually, I don't understand what Professor Downey is saying in his text!"

My personal experience is that these kinds of texts assume you "get it." As a result, they tend to move quickly, and even though they're intended for "beginners," they actually assume a great deal of prior/related experience. 

My goal with the Rosetta Stone is to develop materials that provide examples that a beginner who doesn't "get it" already (which would be *pretty much all of us*) can use to begin working through their first steps towards learning something new. My goal in having you contribute to that work is that it requires you to do three things:

1. Identify what you do not understand.

2. Wrestle with understanding those things.

3. Communicate that understanding to others.

If I had a fourth, it would be:

4. Help improve the learning of others through collaboration and constructive critique.

Practicing #4 helps with #5.

5. Begin developing effective strategies for self-motivated, lifelong learning.

## Three Programs, Three Paragraphs

You've taken on some ideas in your learning group with the intent of exploring them further over the weekend. Using these ideas, we're going to take our first steps with our first topics. You should have each picked one topic to start. For this topic, I'd like you to come back to class having done the following:

1. **Develop the simplest program**. What is the smallest and simplest program you can write that uses the block or concept you're wrestling with. For example, the first program in the Stone is [an empty program](http://lynxruf.us/courses/dcs102w18/cp/rosetta.html).

1. **Develop a slightly more complex program**. How can you add just one thing to this program that illustrates the concept or block further? 

1. **Do it differently**. There's always more than one way to do things in code. Your final example does not have to be "harder" or "more complex." Instead, come up with one more example that explores your concept from a different perspective.

1. **Explain the programs**. Finally, write one paragraph that, to the best of your ability, explain what the first program is doing. Then, write one more paragraph that explains what the second program is doing. Likewise, for the third.

## Prototype a Video

Finally, we're going to *prototype* a screencast that explains our topic. You've seen multiple screencasts that I've produced. Your goal is to create a screencast that is 3-4 minutes long, and explains your topic. You can use the paragraph(s) that you wrote as a script, or you might want to do your explanation differently.

This does not need to be public. We will, however, be sharing it with our learning partners. There are many things that people say about this kind of work:

1. **My voice sounds stupid**. No, it doesn't. You don't hear your voice the way other people do. We all sound different. Only mean people would say your voice sounds stupid. We're not mean people. 

1. **I hate the sound of my voice**. Yes, yes, fine. You're used to hearing your voice through a bunch of meatstuff. That is, your vocal chords vibrate, they vibrate a bunch of muscle and bone and liquid (directly, through physical contact), and then that vibrates your eardrum. Everyone else hears you because your vocal chords vibrate air molecules, and those eventually vibrate our eardrums. So, you hear yourself one way all day long, whereas your video sounds the way *we* hear you. Of course, you hear yourself that way, too, but your brain does some *really, really cool* audio processing to make sense of the fact that you are hearing yourself twice in two different ways, but you don't get an internal echo. It's amazing really, but I digress. You don't *hate* the sound of your voice, you just don't *recognize* the sound of your voice. 

1. **I'm not good at this**. Great. Keep practicing.

1. **I'm never going to do anything like this, ever, in my life**.  You're going to be presenting to people all the time. You talk to groups of people all the time. You're going to be awesome, and in being awesome, probably end up ruling the world... which requires lots of communication. It's all the same thing as recording a screencast, just different in the details.

1. **None of these things are what I'm actually thinking**. Well, you shouldn't be thinking the things you are thinking, that's what I say! And besides, how could you transport that much peanut butter, anyway?

I recommend using [Loom](https://chrome.google.com/webstore/detail/loom-video-recorder-scree/liecbddmkiiihnedobmlmillhodjkdmb/related?hl=en-US). It works with Google Chrome. If you need to install Chrome, do so. I also recommend logging into your Bates account before using it, so that you can log into Loom using your Google credentials. 

Loom will automatically save your video, and you can easily grab a link to the video when you are done. 

I've recorded a 13 second video using Loom. It worked well enough for the purposes of exploring and experimenting.

<div class = "text-center mx-auto d-block">
  <iframe width="630" height="394" src="https://www.useloom.com/embed/23561833dcbd4389a766d0433fe2fe8c" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

## Blog and Share

After you've done this work, embed the programs into your blog, as well as your best attempt at explaining them. Email your compatriots with the link to your post. Also, if you would please, share with them a link to your prototype video. 

Then, if you can give these a read/watch before class, that would be excellent. If not, we'll start class by reading and discussing each-other's work. This does *suggest* that you don't start this work on Monday night.

If you wish, you may embed your prototype video in your blog. In the name of openness, that would be cool. However, **if you're not comfortable sharing a video publicly, then don't**. 

Then, we will (as a class) discuss the things that we saw that worked well for us, things we think we can improve upon, and we'll begin our next iteration on this work as we go into the weekend.

