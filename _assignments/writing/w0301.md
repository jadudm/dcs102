---
week:   "3"
day:    "1"
title:  "Team Update"
type:   "Writing"
slug:   "Are you using good process?"
---

At the end of your first week of collaboration, don't forget to submit your first team update. You can do this as a Google Doc, and submit a PDF of the update.

We also ask you to [complete a peer evaluation for this first week](https://goo.gl/forms/ZArgaWfvBwUe9Jeb2). The questions in the peer eval are based on your own suggestions for questions we should ask. Please complete them for each team member.

As a reminder, here's a short prompt for what your team updates might look like:

----

<em>Your assignment is to create a functioning collaborative team that is ready to show a 1-3 minute creative experiment involving interactive music, movement, and computing using the tools introduced in class by the beginning of class on Feb 6. You should be ready to articulate the relationship between the sensors, computing,  movement and musical compositions and should also have 3 technical or process oriented questions ready for the group</em>. 

* Meet each week (2x)  between now and Feb 6 outside of class
* First meeting is to see the work of the choreographer and to brainstorm possible interactivity + figure out the tools and knowledge you need
* Do whatever work you need to do in between meetings
* Second meeting is to put your performance experiments together

Each student should in in a brief personal report of your work for each week to your faculty member. Include the following:

* When you met and with whom
* What your group was trying to accomplish, what you did, and what questions and tasks were left at the end
* What you contributed to the collaboration
* Any tasks you will take on for the next collaboration
* What questions (artistic, technical, other) were left for you at the end of the work session

Come to class in Muskie archives Feb 6 ready to show one of your experiments, and with 3 of your top questions.  At least one question should be in regards to the creative exploration.  The other two can be technical.

