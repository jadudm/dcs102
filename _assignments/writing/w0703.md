---
week:   "7"
day:    "3"
title:  "Expanding the Stone"
type:   "Writing"
slug:   ""
---

I began work on a *Rosetta Stone*, a document that translates one language to another. In the case of my Rosetta Stone, it attempts to translate the language (and concepts) in MakeCode to the language Python.

**I am going to guess that you are still not confident with Python**. It might be that some of you are starting to feel confident about Python, but overall, I'd bet money that many of you still have a certain amount of uncertainty and doubt. I personally think that's exactly where you should be at this point in the term, given what we've done!

So, for the next few weeks, as we transition out of our collaboration with Dance and Music, we're going to spend a bit more time increasing your confidence with Python. To do that, we'll zero in on things that need more explanation, and that you don't yet understand.

For your blog post for tomorrow, pick 2, 3, or 4 things that you know you'd like to spend more time on. These could be things in MakeCode that you don't yet understand (e.g. there could be loops you haven't explored or used much, or arrays, or functions), or there could just be things that you've already seen in Python that don't yet make sense. For example, 

```
setAllPixelsTo(RED)
``` 

might be an example of some Python that leaves you wondering (amongst other things): 

* Why is it capitalized that way?
* Why is the word RED in parentheses?
* Why are there parentheses there?
* Does it matter if there is a space before the parentheses? Why or why not?
* Why does that do what it does?
* Why isn't it called something else?

These are all *great* questions. And, you might have others. (Why is it "forever" in MakeCode and "while True:" in Python? And, what's with the colon?)

So, blog about some of your questions. Let's get them out there, so we know where we need to focus our research, learning, and writing. Then, if we're lucky, we'll be able to give the results of our learning back to a larger community, and make their lives as learners that much better.

And, as a *challenge*: see if you can make your blog post at least as long and considered as my assignment description. ;)